<?php

namespace App\Http\Controllers\Pokemon;

use App\Http\Controllers\Controller;
use App\Models\Pokemon;
use Illuminate\Http\Request;
use Redirect;


class PokemonController extends Controller
{
    public function delete($id)
    {
      $pokemon = Pokemon::findorFail($id);
      $pokemon->delete();

      return redirect::to('/' );
    }

    public function del($id){
      $pokemon = Pokemon::findorFail($id);

      return view('pokemon.delete',compact('pokemon'));
    }
    public function atualizar($id, Request $request)
    {
        if (\Input::hasfile('img'))
        {
          $file = \Input::file('img');
          $destinationPath = public_path().DIRECTORY_SEPARATOR.'assets\site\img';
          $fileName = $request->numero.".".pathinfo('Hearthstone.png')['extension'];
          $file->move($destinationPath, $fileName);

          $pokemon = Pokemon::findorFail($id);

          $pokemon->update($request->all());

          \Session::flash('mensagem_sucesso', 'Pokemon editado com sucesso!');

          return redirect::to($pokemon->id.'/edit');
        } else
        {
          $pokemon = Pokemon::findorFail($id);

          $pokemon->update($request->all());

          \Session::flash('mensagem_sucesso', 'Pokemon editado com sucesso!');

          return redirect::to($pokemon->id.'/edit');
        }

    }

    public function edit($id)
    {
      $pokemon = Pokemon::findorFail($id);

      return view('pokemon.add', compact('pokemon', 'sexo'));
    }

    //função de busca
    public function search(Request $pokemonsearch)
    {
      if($pokemonsearch->pokemonsearch == null){

        return redirect('/');
      }
      else{
        $pokemonBD = new Pokemon();
        $pokemon = $pokemonBD->where('nome','LIKE', '%'.$pokemonsearch['pokemonsearch'].'%')->get();
        return view('pokemon.search', compact('pokemon'));
      }

    }

    /**
     * [função que lista pokemons no index]
     *
     * @return {[type]} [description]
     */
    public function index()
    {
      $totalPage = 6;
        $pokemonBD = new Pokemon();
        $pokemon = $pokemonBD->paginate($totalPage);

        return view('home', compact('pokemon'));
    }

    public function novo()
    {
        return view('pokemon.add');
    }

    //função que adiciona no banco
    public function add(Request $request)
    {
        if (\Input::hasfile('img')) {
            $file = \Input::file('img');
            $destinationPath = public_path().DIRECTORY_SEPARATOR.'assets\site\img';
            $fileName = $request->numero.".".pathinfo('Hearthstone.png')['extension'];
            $file->move($destinationPath, $fileName);

            $pokemonDB = new Pokemon();
            $pokemon = $pokemonDB->create($request->all());

            \Session::flash('mensagem_sucesso', 'Pokemon cadastrado com sucesso!');

            return redirect::to('/novo');
        } else {
            \Session::flash('mensagem_erro', 'Imagem não foi adicionada!');
        }
        return redirect::to('/novo');
    }
    //funão para mostrar detalhes do pokemon
    public function detalhes($nome)
    {
        $pokemonBD = new Pokemon();

        $pokemon = $pokemonBD->where('nome', $nome)->get();
        return view('pokemon.detalhes', compact('pokemon'));
    }
}
