<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class LoginFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:60',
            'password' => 'required|min:5'
        ];
    }
    /**
     *Mensagens de erro
     *
     * @return array
     */
     public function messages(){
       return[
         'email.required' => 'Insira um endereço de email'
         'password.required' => 'Digite a senha da sua conta'
       ];
     }
}
