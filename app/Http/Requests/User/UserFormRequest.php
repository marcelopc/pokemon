<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *regras de validação
     *
     * @return array
     */
    public function rules()
    {
        return [

                'name' => 'required|min:3|max:100',
                'email' => 'required|max:60',
                'password' => 'required|min:5'
        ];
      /**
       *Mensagens de erro
       *
       * @return array
       */
       public function messages(){
         return[
           'email.required' => ''
           'password.required' => ''
         ]
       }
    }
}
