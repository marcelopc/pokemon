<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
  protected $table = 'Pokemons';
  //desativa o timestamps
  public $timestamps = false;
  protected $fillable = [
      'nome',
      'numero',
      'categoria',
      'altura',
      'peso',
      'sexo',
      'tipo',
      'fraqueza',
    ];
}
