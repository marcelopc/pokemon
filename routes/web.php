<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

route::group(['middleware' => 'web'], function () {
    Route::get('/', 'Pokemon\PokemonController@index');
    Route::get('/novo', 'Pokemon\PokemonController@novo');
    Route::get('/{pokemons}', 'Pokemon\PokemonController@detalhes');
    Route::get('/{pokemons}/edit', 'Pokemon\PokemonController@edit');
    Route::get('/{pokemons}/delete', 'Pokemon\PokemonController@del');

    Route::post('/{pokemons}', 'Pokemon\PokemonController@search');
    Route::post('/novo/add', 'Pokemon\PokemonController@add');
    Route::patch('/{pokemons}', 'Pokemon\PokemonController@atualizar');
    Route::delete('{pokemons}/delete', 'Pokemon\PokemonController@delete');

});
