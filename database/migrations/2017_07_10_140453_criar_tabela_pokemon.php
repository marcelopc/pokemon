<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaPokemon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pokemons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero',250);
            $table->string('nome',250);
            $table->string('categoria',250);
            $table->float('altura')->unsigned();
            $table->float('peso')->unsigned();
            $table->enum('sexo',['M','F','MF']);
            $table->string('tipo',250);
            $table->string('fraqueza',250);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pokemons');
    }
}
