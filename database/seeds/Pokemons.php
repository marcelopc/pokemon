<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class Pokemons extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('Pokemons')->insert([
        'numero'=>'6',
        'nome'=>'Charizard',
        'categoria'=>'Flame',
        'altura'=>'1.7',
        'peso'=>'90.5',
        'sexo'=>'MF',
        'tipo'=>'Fire, Flying',
        'fraqueza'=>'Rock, Eletric, Water',
      ]);
      DB::table('Pokemons')->insert([
        'numero'=>'7',
        'nome'=>'Squirtle',
        'categoria'=>'Tiny Turtle',
        'altura'=>'0.5',
        'peso'=>'9.0',
        'sexo'=>'MF',
        'tipo'=>'Water',
        'fraqueza'=>'Eletric, Grass',
      ]);
      DB::table('Pokemons')->insert([
        'numero'=>'8',
        'nome'=>'Wartortle',
        'categoria'=>'Turtle',
        'altura'=>'1.0',
        'peso'=>'22.5',
        'sexo'=>'MF',
        'tipo'=>'Water',
        'fraqueza'=>'Eletric, Grass',
      ]);
      DB::table('Pokemons')->insert([
        'numero'=>'9',
        'nome'=>'Blastoise',
        'categoria'=>'Shellfish',
        'altura'=>'1.6',
        'peso'=>'85.5',
        'sexo'=>'MF',
        'tipo'=>'Water',
        'fraqueza'=>'Eletric, Grass',
      ]);




    }
}
