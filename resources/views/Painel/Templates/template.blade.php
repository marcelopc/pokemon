<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- CSS -->
    <link rel="stylesheet" href="{{url('assets/painel/css/style.css')}}">
    <!-- Última versão CSS compilada e minificada Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>{{$title or 'Painel Produtos'}}</title>
  </head>
  <body>
    @yield('content')
  </body>
</html>
