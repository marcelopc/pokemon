@extends('painel.templates.template')

  @section('content')
        <table class="table table-striped text-center">
          <tr>
            <th>ID Prod</th>
            <th>Descrição</th>
            <th>Ref.Barras</th>
            <th>Registro</th>
            <th>UND</th>
            <th>Marca</th>
            <th>Categoria</th>
            <th>Secao</th>
            <th>Tipo</th>
            <th>Classificação</th>
            <th>Grupo</th>
            <th>Ação</th>
            <th>Antidotos</th>
            <th>Composições</th>
            <th>Sintoma</th>
            <th>Praga</th>
            <th>Qtd/área</th>
            <th>Custo R$</th>
            <th>Venda R$</th>
            <th>Status</th>
            <th>Especificações</th>
            <th>características</th>
            <th>id_emp</th>
            <th>id_conc</th>
            <th>ações</th>
          </tr>

          @foreach ($products as $product )
            <tr>
              <td>{{$product->ID_PROD}}</td>
              <td>{{$product->DS_PROD}}</td>
              <td>{{$product->referencia}}</td>
              <td>{{$product->REGISTRO}}</td>
              <td>{{$product->UND}}</td>
              <td>{{$product->id_marca}}</td>
              <td>{{$product->id_categoria}}</td>
              <td>{{$product->id_secao}}</td>
              <td>{{$product->id_tipo}}</td>
              <td>{{$product->CLASSIFICACAO}}</td>
              <td>{{$product->ID_GRUPO}}</td>
              <td>{{$product->ID_ACAO}}</td>
              <td>{{$product->ID_ANTI}}</td>
              <td>{{$product->ID_COMP}}</td>
              <td>{{$product->ID_SINT}}</td>
              <td>{{$product->ID_PRAGA}}</td>
              <td>{{$product->QTDE_POR_AREA}}</td>
              <td>{{$product->pc_custo}}</td>
              <td>{{$product->pc_venda}}</td>
              <td>{{$product->status}}</td>
              <td>{{$product->especificacoes}}</td>
              <td>{{$product->caracteristicas}}</td>
              <td>{{$product->id_emp}}</td>
              <td>{{$product->ID_CONC}}</td>
              <td>

                <a href="{{route('produtos.edit',$product->ID_PROD)}}" class=""><span class="glyphicon glyphicon-pencil"></span></a>
                {!! Form::open(['route' => ['produtos.destroy', $product->ID_PROD],'method'=>'delete'])!!}
                <button type="submit" name="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                {!! Form::close()!!}
              </td>
            </tr>
          @endforeach

        </table>

<a href="{{route('produtos.index')}}" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left"></span></a>
  @endsection
