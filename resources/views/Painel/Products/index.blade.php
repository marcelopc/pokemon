@extends('painel.templates.template')

  @section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-6">
        {!! Form::open(['class' => 'form'])!!}
        {!! Form::text('name',null,['class'=>'form-control right','placeholder'=>'Busca'])!!}
        {!! Form::submit('OK',['class'=>'btn btn-primary'])!!}
        {!! Form::close()!!}
      </div>
      <div class="col-xs-12">
        <table class="table table-striped">
          <tr>
            <th>ID Prod</th>
            <th>Descrição</th>
            <th>Und</th>
            <th>Classificação</th>
            <th>Qtde por área</th>
          </tr>

          @foreach ($products as $product )
            <tr>
              <td>{{$product->ID_PROD}}</td>
              <td>{{$product->DS_PROD}}</td>
              <td>{{$product->UND}}</td>
              <td>{{$product->CLASSIFICACAO}}</td>
              <td>{{$product->QTDE_POR_AREA}}</td>
              <td>

                <a href="{{route('produtos.edit',$product->ID_PROD)}}" class=""><span class="glyphicon glyphicon-pencil"></span></a>
                <a href="{{route('produtos.show',$product->ID_PROD)}}" class=""><span class="glyphicon glyphicon-eye-open"></span></a>
                {!! Form::open(['route' => ['produtos.destroy', $product->ID_PROD],'method'=>'delete'])!!}
                <button type="submit" name="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                {!! Form::close()!!}
            </td>
          </tr>
          @endforeach

        </table>
        {!! $products->links()!!}
      </div>
    </div>
  </div>
  @endsection
