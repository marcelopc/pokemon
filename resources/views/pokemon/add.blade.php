
@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-10">
        @if(Session::has('mensagem_sucesso'))
          <div class="alert alert-success">
            {{Session::get('mensagem_sucesso')}}
          </div>
        @elseif(Session::has('mensagem_erro'))
          <div class="alert alert-danger">
            {{Session::get('mensagem_erro')}}
          </div>
        @endif


        @if (Request::is('*/edit'))
          <div class="cont-left">
              <img src={{url("assets/site/img/".$pokemon->numero.'.png')}} class="media-object img-tam">
          </div>
          {!! Form::model($pokemon,['method' => 'PATCH','url' => '/'.$pokemon->id,'files' => true])!!}

        @else
          {!!Form::open(['url'=>'/novo/add','enctype'=>'multipart/form-data'])!!}
        @endif


      <div class="cont-right">
        <div class="panel panel-default panel-form">
          <div class="panel-body">
            <div class="input-group add-input">
              <span class="input-group-addon" for="nome">Nome</span>
              {!!Form::text('nome',null,['class'=>'form-control','autofocus','aria-describedby'=>'nome','required'])!!}
            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="numero">Numero</span>
              {!!Form::text('numero',null,['class'=>'form-control','aria-describedby'=>'numero','required'])!!}
            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="categoria">Categoria</span>
              {!!Form::text('categoria',null,['class'=>'form-control','aria-describedby'=>'categoria','required'])!!}
            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="altura">Altura</span>
              {!!Form::text('altura',null,['class'=>'form-control','aria-describedby'=>'altura','required'])!!}
            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="peso">Peso</span>
              {!!Form::text('peso',null,['class'=>'form-control','aria-describedby'=>'peso','required'])!!}
            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="sexo">Sexo</span>
              @if (Request::is('*/edit'))
                <select class="form-control" name="sexo">

                  @if ($pokemon->sexo == "F")
                  <option value="F" selected>Feminino</option>
                  <option value="M">Masculino</option>
                  <option value="MF" >Masculino e Feminino</option>

                @elseif ($pokemon->sexo == "M")
                  <option value="F">Feminino</option>
                  <option value="M" selected>Masculino</option>
                  <option value="MF">Masculino e Feminino</option>

                @elseif ($pokemon->sexo == "MF")
                  <option value="F">Feminino</option>
                  <option value="M">Masculino</option>
                  <option value="MF" selected>Masculino e Feminino</option>
                  @endif
                </select>
              @else
                <select class="form-control" name="sexo">
                  <option value="F">Feminino</option>
                  <option value="M">Masculino</option>
                  <option value="MF">Masculino e Feminino</option>
                </select>
              @endif

            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="tipo">Tipo</span>
              {!!Form::text('tipo',null,['class'=>'form-control','aria-describedby'=>'tipo','required'])!!}
            </div>

            <div class="input-group add-input">
              <span class="input-group-addon" id="fraqueza">Fraqueza</span>
              {!!Form::text('fraqueza',null,['class'=>'form-control','aria-describedby'=>'fraqueza','required'])!!}
            </div>

            <div class="input-group add-input">
              {!!Form::file('img',null,['class'=>'form-control'])!!}
            </div>

            {!!form::button('Enviar',['type'=>'submit','class'=>'btn btn-primary'])!!}
            {!!Form::close()!!}
          </div>
        </div>
      </div>

      </div>
    </div>
  </div>


@endsection
