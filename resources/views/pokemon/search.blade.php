@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">

                  @if (!Auth::guest())
                    <div class="col-md-4">
                      <a href="{!!url('/novo')!!}" class="btn btn-primary">Adicionar Pokemon</a>
                    </div>
                  @endif

                  {!! Form::open(['url'=>'/search'])!!}

                    <div class="input-group">

                      {!! Form::text('pokemonsearch',null,['class'=>'cont-search form-control ','aria-describedby'=>'search','placeholder'=>'Pesquisar'])!!}

                      <span class="input-group-btn">
                        {!!form::button('',['type'=>'submit','class'=>'pesquisa btn btn-default glyphicon glyphicon-search'])!!}
                      </span>

                    </div>

                  {!! Form::close()!!}
                </div>

                <div class="panel-body">
                  Lista de pokemons

                    <div class="row">
                        @foreach ($pokemon as $pokemons)
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src="assets\site\img\{!!$pokemons->numero!!}.png" alt="...">
                          <div class="caption">
                            <h3>{!!$pokemons->nome!!}</h3>
                            <p>Numero:{!!str_pad($pokemons->numero, 3, '0', STR_PAD_LEFT)!!} </p>
                            <p><a href="/{{$pokemons->nome}}" class="btn btn-primary" role="button">Detalhes</a></p>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
