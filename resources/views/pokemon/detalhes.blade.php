@extends('layouts.app')
<!--modificar pagina search. quando fizer uma busca emviar para pagina do pokemon e não search!!!-->
@section('content')

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      @foreach ($pokemon as $poke )
      <img src="assets/site/img/{!!$poke->numero!!}.png" alt="{!!$poke->nome!!}" class="searchimg">

      <div class="panel panel-default panel-status">
        <div class="panel-heading panel-heading-status">
          <h3 class="panel-title panel-nome">{!!$poke->nome!!}</h3>
          <h3 class="panel-title panel-numero">Nº:{!!str_pad($poke->numero, 3, '0', STR_PAD_LEFT)!!}</h3>
          @if (!Auth::guest())
            {!! Form::open(['method' => 'DELETE', 'url' =>$poke->id.'/delete'])!!}
            {!! Form::button('',['type'=>'submit', 'class'=>'glyphicon glyphicon-trash delete']) !!}
            {!! Form::close()!!}
            <a href="{{$poke->id}}/edit"><span class="glyphicon glyphicon-pencil a-editar"></span></a>
          @endif

        </div>
        <div class="panel-body ">
          <ul>
            <li class="li-altura">
              <p class="p-altura">Altura</p>
              <p class="p-altura-value">{!!$poke->altura!!} m</p>
            </li>
            <li class="li-peso">
              <p class="p-peso">Peso</p>
              <p class="p-peso-value">{!!$poke->peso!!}</p>
            </li>
            <li class="li-categoria">
              <p class="p-categoria">Categoria</p>
              <p class="p-categoria-value">{!!$poke->categoria!!}</p>
            </li>
            <li class="li-sexo">
              <p class="p-sexo">Sexo</p>
              @if ($poke->sexo == 'F')
                <p class="p-sexo-value">
                  <i class="fa fa-venus " aria-hidden="true"></i>
                </p>
              @elseif ($poke->sexo == 'M')
                <p class="p-sexo-value">
                  <i class="fa fa-mars" aria-hidden="true"></i>
                </p>
              @elseif ($poke->sexo == 'MF')
                <p class="p-sexo-value">
                  <i class="fa fa-venus-mars" aria-hidden="true"></i>
                </p>
              @endif
            </li>
            <li class="li-tipo">
              <p class="p-tipo">Tipo</p>
              <?php
                $pieces = explode(", ", $poke->tipo);
                $tamanho=count($pieces);
                for ($i=0; $i < $tamanho ; $i++) {
                      echo "<li class='container-{$pieces[$i]}'>
                              <p class='p-tipo-value'>{$pieces[$i]}</p>
                            </li>";
                          }
              ?>


            </li>
            <li class="li-fraqueza">
              <p class="p-fraqueza">Fraqueza</p>
              <?php
                $pieces = explode(", ", $poke->fraqueza);
                $tamanho=count($pieces);
                for ($i=0; $i < $tamanho ; $i++) {
                      echo "<li class='container-{$pieces[$i]}'>
                              <p class='p-tipo-value'>{$pieces[$i]}</p>
                            </li>";
                          }
              ?>
            </li>
          </ul>
        </div>
      </div>


      @endforeach
    </div>
  </div>
</div>

@endsection
